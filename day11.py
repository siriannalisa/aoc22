#!/usr/bin/env python3

import math
import operator

OP = {
    '+': operator.add,
    '-': operator.sub,
    '*': operator.mul
}

class Monkey():
    def __init__(self, description, level):
        self.inspection = 0
        self.description = description
        self.level_fnc = level
        self.parse()

    def parse(self):
        lines = self.description.split('\n')
        self.monkey = int(lines[0].strip().split()[1][:-1])
        self.items = list(map(int, lines[1].split(':')[1].split(',')))
        self.op = lines[2].split()[-2]
        self.op_arg = lines[2].split()[-1]
        self.test = int(lines[3].split()[-1])
        self.true_branch = int(lines[4].split()[-1])
        self.false_branch = int(lines[5].split()[-1])

    def set_monkey_ref(self, m):
        self.monkeys = m

    def update(self):
        for i in self.items:
            self.inspection += 1
            if 'old' in self.op_arg:
                worry = OP[self.op](i, i)
            else:
                worry = OP[self.op](i, int(self.op_arg))
            worry = self.level_fnc(worry)
            if worry % self.test == 0:
                self.monkeys[self.true_branch].throw(worry)
            else:
                self.monkeys[self.false_branch].throw(worry)
        self.items = []

    def throw(self, item):
        self.items.append(item)


def main():
    with open("input11.txt") as fd:
        data = fd.read().split('\n\n')

    ## Part 1
    monkeys = [Monkey(i, level=(lambda x: x//3)) for i in data]
    for m in monkeys: m.set_monkey_ref(monkeys)

    for _ in range(20):
        for m in monkeys: m.update()

    print(math.prod(sorted([m.inspection for m in monkeys])[-2:]))

    ## Part 2
    mod = math.prod(m.test for m in monkeys)
    print(f"mod {mod}")

    # Modulo trick - Thanks Numberphile 
    monkeys = [Monkey(i, level=(lambda x: x%mod)) for i in data]
    for m in monkeys: m.set_monkey_ref(monkeys)

    for _ in range(10000):
        for m in monkeys: m.update()

    print(math.prod(sorted([m.inspection for m in monkeys])[-2:]))

if __name__ == "__main__":
    main()
