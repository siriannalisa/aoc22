#!/usr/bin/env python3

import sys
import numpy as np

def left(x, idx):
    return all([i<x[idx] for i in x[:idx]])

def right(x, idx):
    return all([i<x[idx] for i in x[::-1][:len(x)-idx-1]])

def top(x, idx):
    return all([i<x[idx] for i in x[:idx]])

def bot(x, idx):
    return all([i<x[idx] for i in x[::-1][:len(x)-idx-1]])

def main():
    with open("input08.txt") as fd:
        data= [list(map(int,list(line.strip()))) for line in fd]

    data = np.array(data, dtype=np.int8).reshape([99,99])
    perimeter = 2*data.shape[1]+2*(data.shape[0]-2)

    cnt = 0
    for h in range(1, data.shape[0]-1):
        horiz = data[h, :]
        for w in range(1, data.shape[1]-1):
            verti = data[:, w]
            if any([top(verti, h), left(horiz, w), right(horiz, w), bot(verti, h)]):
                    cnt += 1
    print(cnt + perimeter)

if __name__ == "__main__":
    main()