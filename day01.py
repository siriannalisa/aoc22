#!/usr/bin/env python3

import itertools

def main():
    with open("input01.txt") as fd:
        calories = [line.strip() if line is not None else '' for line in fd]

    spl = [list(y) for x, y in itertools.groupby(calories, lambda z: z == '') if not x]
    max_sum = sorted([sum(map(int, i)) for i in spl])
    print(max_sum[-1], sum(max_sum[-3:]))

if __name__ == "__main__":
    main()
