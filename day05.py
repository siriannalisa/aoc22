#!/usr/bin/env python3

import re

def main():
    with open("input05.txt") as fd:
        stacks, cmds = fd.read().split('\n\n')

    stacks = list(
        "".join(x).strip()[1:]
        for i, x in enumerate(
            zip(*map(list, stacks.split("\n")[::-1]))
        )
        if i % 4 == 1
    )

    cmds = [
        tuple(map(int, re.findall(r"\d+", cmd)))
           for cmd in cmds.strip().split("\n")
    ]

    def cratemover(m9001 = False):
        _stacks = [None] + stacks[:]
        for N, a, b in cmds:
            _stacks[a], m = _stacks[a][:-N], _stacks[a][-N:]
            if not m9001:
                m = m[::-1]
            _stacks[b] = _stacks[b] + m
        return "".join(stack[-1] for stack in _stacks[1:])

    print(cratemover())
    print(cratemover(m9001=True))


if __name__ == "__main__":
    main()
