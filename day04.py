#!/usr/bin/env python3

import re

def main():
    pairs = 0
    contains = 0
    with open("input04.txt") as fd:
        for line in fd:
            l1, h1, l2, h2 = list(map(int, re.split('[,-]', line.strip())))
            pairs += (l1 <= l2 and h2 <= h1) or (l2 <= l1 and h1 <= h2)
            contains += not (l1 > h2 or l2 > h1)
    print(pairs, contains)

if __name__ == "__main__":
    main()
