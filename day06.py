#!/usr/bin/env python3

def main():
    with open("input06.txt") as fd:
        data = list(fd.readline().strip())

    def get_seq(d, s):
        for n, _ in enumerate(d):
            if len(set(data[n:n+s])) == s:
                print(n+s)
                break

    get_seq(data, 4)
    get_seq(data, 14)

if __name__ == "__main__":
    main()