#!/usr/bin/env bash

cat << EOF > day${1}.py
#!/usr/bin/env python3

def main():
    with open("input${1}.txt") as fd:
        data= [line.strip() for line in fd]

if __name__ == "__main__":
    main()
EOF

touch input${1}.txt test${1}.txt
chmod 755 day${1}.py
