#!/usr/bin/env python3

import string

def main():
    lower = dict(zip(string.ascii_lowercase, range(1, 27)))
    upper = dict(zip(string.ascii_uppercase, range(27, 53)))
    priority = {**lower, **upper}

    def get_intersection(l1, l2, l3=None):
        return set(set(l1) & set(l2) & set(l3)).pop() if l3 else set(set(l1) & set(l2)).pop()

    with open("input03.txt") as fd:
        rucksacks = [line.strip() for line in fd]

    print(sum(
        [priority[get_intersection(i[0:len(i)//2], i[len(i)//2:len(i)])] for i in rucksacks]
    ))

    print(sum(
        [priority[get_intersection(*rucksacks[i:i+3])]
            for i in range(0, len(rucksacks), 3)]
    ))

if __name__ == "__main__":
    main()
