#!/usr/bin/env python3

"""
A ROCK    X 1
B PAPER   Y 2
C SCISSOR Z 3
"""

def main():
    rules = {"AX": 3, "BX": 0, "CX": 6, "AY": 6, "BY": 3, "CY": 0, "AZ": 0, "BZ": 6, "CZ": 3}
    score1 = {"X": 1, "Y": 2, "Z": 3}
    score2 = {"X": 0, "Y": 3, "Z": 6}
    dispatch = {
        "X": {"A": "Z", "B": "X", "C": "Y"},  # lose
        "Y": {"A": "X", "B": "Y", "C": "Z"},  # draw
        "Z": {"A": "Y", "B": "Z", "C": "X"}   # win
    }

    with open("input02.txt") as fd:
        game = [line.split() for line in fd]

    print(sum([rules["".join(p)]+score1[p[1]] for p in game]))
    print(sum([score1[dispatch[p[1]][p[0]]]+score2[p[1]] for p in game]))

if __name__ == "__main__":
    main()
